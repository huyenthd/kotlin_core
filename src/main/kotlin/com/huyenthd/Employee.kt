package com.huyenthd

class Employee(
        var name: String = "",
        var role: String = "",
        var age: Int = 0,
        var callback: (() -> Unit)? = {},
        var callback1: (a: String) -> Int = { input -> 1 }
) {

    fun handle() {
        for (i in 0..3) {
            print(i)
            Thread.sleep(1000)
        }
        callback?.invoke()
        callback1.invoke("4")
    }

    fun handle2132123() {
        callback?.invoke()
    }

    override fun toString(): String {
        return "Employee(name='$name', role='$role', age=$age)"
    }
}


fun main(args: Array<String>) {
    val employeeA = Employee(name = "A", age = 2)
    employeeA.callback1 = { a: String ->
        a.toInt()
    }

    val employeeB = Employee(name = "B", age = 23)
    employeeB.callback = object : () -> Unit {
        override fun invoke() {

        }

    }

    employeeA.handle()
    employeeB.handle()

}
